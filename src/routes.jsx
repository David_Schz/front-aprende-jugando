import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import LogIn from './components/Auth/LogIn';
import SignUp from './components/Auth/SignUp';
import Index from './pages/index';

export default function Routes() {
    return (
    <Router>
        <Switch>
            <Route exact path="/" component={Index} />
            <Route exact path="/adios" component={SignUp} />
            <Route exact path="/hola" component={LogIn} />
        </Switch>
    </Router>
    )
} 